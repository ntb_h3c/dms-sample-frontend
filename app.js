/**
 * Created by root on 6/28/17.
 */
angular.module('dms',[
    'dms.routes',
    //controllers
    'dms.homeController',
    'dms.movieController',
    //services
    'dms.movieServices'
])