/**
 * Created by root on 6/28/17.
 */
angular.module('dms.homeController',[])

.controller('homeController',['Movie','MovieData', '$location','$timeout', function(Movie, MovieData, $location,$timeout){
    const app = this;
    app.msg = "hello";

    // Movie.getAllMovies().then(function(data){
    //     app.movies = data.data.hits.hits;
    //     // console.log(app.movies);
    //     // app.movies = data;
    //     // console.log(data);
    // })

    app.getAllMovies = function(){
        Movie.getAllMovies().then(function(data){
            app.movies = data.data.hits.hits;
            // console.log(app.movies);
            // app.movies = data;
            // console.log(data);
        })
    }

    //get all movies on page load
    app.getAllMovies();

    app.getMoviesByTitle = function(title){
        if(title==""||title==undefined){
            Movie.getAllMovies().then(function(data){
                app.movies = data.data.hits.hits;
            })
        }else{
            Movie.getMoviesByTitle(title).then(function(data){
                app.movies = data.data.hits.hits;
            })
        }
    }

    app.goToUpdateView = function(data){
        MovieData.setData(data);
        $location.path('/updateMovie');
    }

    //delete movie
    app.delete = function(imdb){
        Movie.deleteMovie(imdb).then(function(data){
            app.error_msg = null;
            app.success_msg = "Movie deleted";
            app.getAllMovies();
            $timeout(function(){
                app.success_msg = null;
            },2000);
        }).catch(function(err){
            app.success_msg = null;
            app.error_msg = "Could not delete movie";
            $timeout(function(){
                app.error_msg = null;
            },2000);
        })
    }

    //search functions
    app.setSearchBy = function(field){
        app.searchBy = field;
    }

    app.searchKey = function(text){
        app.imdb_search = "";
        app.title_search = "";
        app.country_search = "";
        if(app.searchBy=="imdb")
            app.imdb_search=text;
        else if(app.searchBy=="title")
            app.title_search = text;
        else if(app.searchBy=="country")
            app.country_search = text;
        else
            app.title_search = text;
    }
}])