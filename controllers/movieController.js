/**
 * Created by root on 7/6/17.
 */
angular.module('dms.movieController',[])

.controller('newMovieController', ['Movie','$timeout','$location', function(Movie,$timeout,$location){
    const app = this;

    app.data = {};

    //add new movie
    app.add = function(){
        app.error_msg = null;
        if(app.data.imdb==""||app.data.imdb==undefined)
            app.error_msg = "Please enter an IMDB Number";
        else if(app.data.title==""||app.data.title==undefined)
            app.error_msg = "Please enter a title";
        else if(app.data.releaseCountry==""||app.data.releaseCountry==undefined)
            app.error_msg = "Please enter a country";
        else if(app.data.releaseDate==""||app.data.releaseDate==undefined)
            app.error_msg = "Please enter a release date";
        else{
            Movie.add(app.data).then(function(data){
                console.log(data);
                if(data.data.created){
                    app.success_msg = "Movie added!";
                    $timeout(function(){
                        $location.path('/');
                    },2000)
                }else{
                    app.error_msg = "Could not add movie. Please contact support."
                    $timeout(function(){
                        $location.path('/');
                    },2000)
                }
            })
        }
    }
}])

.controller('updateMovieController', ['Movie','MovieData','$location','$timeout', function(Movie,MovieData,$location,$timeout){
    const app = this;

    app.data = {};

    //get the selected movie data
    app.movie = MovieData.getData();
    if(app.movie==null||app.movie==undefined)
        $location.path('/');
    else
        app.movie_data = app.movie._source;

    //update the selected movie
    app.update = function(){
        app.error_msg = null;
        if(app.movie_data.imdbId==""||app.movie_data.imdbId==undefined)
            app.error_msg = "Please enter an IMDB Number";
        else if(app.movie_data.title==""||app.movie_data.title==undefined)
            app.error_msg = "Please enter a title";
        else if(app.movie_data.releaseCountry==""||app.movie_data.releaseCountry==undefined)
            app.error_msg = "Please enter a country";
        else if(app.movie_data.releaseDate==""||app.movie_data.releaseDate==undefined)
            app.error_msg = "Please enter a release date";
        else{
            Movie.update(app.movie._id,app.movie_data).then(function(data){
                if(data.data.result=="updated"){
                    app.success_msg = "Movie updated!";
                    $timeout(function(){
                        $location.path('/');
                    },2000)
                }else{
                    app.error_msg = "Could not update movie. Please contact support."
                    $timeout(function(){
                        $location.path('/');
                    },2000)
                }
            })
        }
    }

}])