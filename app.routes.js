/**
 * Created by root on 6/28/17.
 */
angular.module('dms.routes',['ngRoute'])

.config(['$routeProvider', function($routeProvider){
    $routeProvider
        .when('/',{
            templateUrl: 'templates/home.html',
            controller: 'homeController as home'
        })

        .when('/newMovie',{
            templateUrl: 'templates/newMovie.html',
            controller: 'newMovieController as new'
        })

        .when('/updateMovie',{
            templateUrl: 'templates/updateMovie.html',
            controller: 'updateMovieController as update'
        })
}])