/**
 * Created by root on 7/6/17.
 */
angular.module('dms.movieServices',[])

.factory('Movie', ['$http', function($http){
    const movieFac = {};

    //retrieve all movies
    movieFac.getAllMovies = function(){
        return $http.get('http://localhost:8008/movies').then(function(data){
            return data;
        }).catch(function(err){
            return err;
        })
    }

    //get movies by released country
    movieFac.getMovieByReleaseCountry = function(country){
        return $http.get('http://localhost:8008/movies/country/'.concat(country)).then(function(data){
            return data;
        }).catch(function(err){
            return err;
        })
    }

    //get movies by title
    movieFac.getMoviesByTitle = function(title){
        return $http.get('http://localhost:8008/movies/title/'.concat(title)).then(function(data){
            return data;
        }).catch(function(err){
            return err;
        })
    }

    //delete movie
    movieFac.deleteMovie = function(id){
        return $http.delete('http://localhost:8008/movies/'.concat(id)).then(function(data){
            return data;
        }).catch(function(err){
            return err;
        })
    }

    //add new movie
    movieFac.add = function(data){
        return $http.post('http://localhost:8008/movies',data).then(function(data){
            return data;
        }).catch(function(err){
            return err;
        })
    }

    //update movie
    movieFac.update = function(id, data){
        return $http.put('http://localhost:8008/movies/'.concat(id),data).then(function(data){
            return data;
        }).catch(function(err){
            return err;
        })
    }

    return movieFac;
}])

    //service to share data between views
.factory('MovieData', [function(){
    const dataFac = {};
    var movieData;

    dataFac.setData = function(data){
        movieData = data;
    }

    dataFac.getData = function(){
        return movieData;
    }

    return dataFac;
}])